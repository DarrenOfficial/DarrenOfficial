<p align="center"> <img src="https://komarev.com/ghpvc/?username=DarrenOfficial&label=Profile%20views&color=0e75b6&style=flat" alt="DarrenOfficial Profie Views" /> </p>

### Hello world, I'm Darren, a system administrator 👨‍💻
I like programming, networking and watching data go brrr


- 🔭 I’m currently working on my guitar skills
- 🚀 Sometimes I'm slow on everywhere else but email, because email notification works.
- 💬 Ask me about Network deployment, and any server related stuff 
- 📫 How to reach me: [me [at] darrennathanael [dot] com](mailto:me@darrennathanael.com) 
- ⛄️ Pronouns: He/Him
- 🍪 Fun fact: Headers are a way to send hidden message.
- 🍻 Beer.


# Contact Me:

![dacord](https://discord.c99.nl/widget/theme-4/508296903960821771.png)

- Discord: [DarrenOfficial#3451](https://discord.darrennathanael.com)
- Reddit: [DarrenOfficial](https://reddit.com/u/DarrenOfficiallol)
- Github: [DarrenOfficial](https://github.com/DarrenOfficial)
- Twitter: [darrenuselinux](https://twitter.com/darrenuselinux)
